This is just a simple demo of my current style of organizing projects. The top level directory contains a file that does nothing more than call the actual main function. This main function is within a package. This allows me to keep the top level directory nice and tidy so there is no ambiguity about which file launches the game. 

-Mek