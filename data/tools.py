import pygame as pg
import os

class Control(object):
    def __init__(self,caption):
        self.screen = pg.display.get_surface()
        self.caption = caption
        self.done = False
        self.Clock = pg.time.Clock()
        self.fps = 60
        self.show_fps = True
        self.keys = pg.key.get_pressed()
        self.mouse = pg.mouse.get_pressed()
        self.state_dict = {}
        self.state_name = None
        self.State = None
    def setup_states(self,state_dict,start_state):
        self.state_dict = state_dict
        self.state_name = start_state
        self.State = self.state_dict[self.state_name]
    def update(self):
        if self.State.quit:
            self.done = True
        elif self.State.done:
            self.flip_state()
        self.State.update(self.screen,self.keys,self.mouse)
    def flip_state(self):
        previous,self.state_name = self.state_name,self.State.next
        persist = self.State.cleanup()
        self.State = self.state_dict[self.state_name]
        self.State.startup(persist)
        self.State.previous = previous

    def event_loop(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.done = True
            elif event.type in (pg.KEYDOWN,pg.KEYUP):
                self.keys = pg.key.get_pressed()
                self.toggle_show_fps()
            elif event.type in (pg.MOUSEBUTTONDOWN,pg.MOUSEBUTTONUP):
                self.mouse = pg.mouse.get_pressed()
            self.State.get_event(event)

    def toggle_show_fps(self):
        if self.keys[pg.K_F5]:
            self.show_fps = not self.show_fps
        if not self.show_fps:
            pg.display.set_caption(self.caption)

    def main(self):
        while not self.done:
            self.event_loop()
            self.update()
            pg.display.update()
            self.Clock.tick(self.fps)
            if self.show_fps:
                with_fps = "{} - {:.2f} FPS".format(self.caption,self.Clock.get_fps())
                pg.display.set_caption(with_fps)

class _State(object):
    def __init__(self):
        self.start_time = pg.time.get_ticks()
        self.done = False
        self.quit = False
        self.next = None
        self.previous = None
        self.persist = {}
    def get_event(self,event,keys,mouse):
        pass
    def startup(self,persistant):
        self.persist = persistant
        self.start_time = pg.time.get_ticks()
    def cleanup(self):
        self.done = False
        return self.persist
    def update(self,Surf):
        pass

### Resource loading functions.
def load_all_gfx(directory,colorkey=(255,0,255),accept=(".png",".jpg",".bmp")):
    graphics = {}
    for pic in os.listdir(directory):
        name,ext = os.path.splitext(pic)
        if ext.lower() in accept:
            img = pg.image.load(os.path.join(directory,pic))
            if img.get_alpha():
                img = img.convert_alpha()
            else:
                img = img.convert()
                img.set_colorkey(colorkey)
            graphics[name]=img
    return graphics

def load_all_music(directory,accept=(".wav",".mp3",".ogg",".mdi")):
    songs = {}
    for song in os.listdir(directory):
        name,ext = os.path.splitext(song)
        if ext.lower() in accept:
            songs[name] = os.path.join(directory,song)
    return songs

def load_all_fonts(directory,accept=(".ttf",)):
    return load_all_music(directory,accept)

def load_all_sfx(directory,accept=(".wav",".mp3",".ogg",".mdi")):
    effects = {}
    for fx in os.listdir(directory):
        name,ext = os.path.splitext(fx)
        if ext.lower() in accept:
            effects[name] = pg.mixer.Sound(pg.os.path.join(directory,fx))
    return effects