import pygame as pg
from .. import setup as su,tools

class Title(tools._State):
    def __init__(self):
        tools._State.__init__(self)
        self.logo = su.GFX["logo"]
        self.logo_rect = self.logo.get_rect(center=su.SCREEN_RECT.center)
        self.logo_angle = 0
        self.title = self.render_font("Fixedsys500c",40,"Title Screen")
        self.title_rect = self.title.get_rect(center=(su.SCREEN_RECT.centerx,75))
        self.ne_key = self.render_font("Fixedsys500c",20,"[Press Any Key]",(255,255,0))
        self.ne_key_rect = self.ne_key.get_rect(center=(su.SCREEN_RECT.centerx,500))
        self.blink = False
        self.timer = 0.0

    def render_font(self,font,size,msg,color=(255,255,255)):
        RenderFont = pg.font.Font(su.FONTS[font],size)
        return RenderFont.render(msg,1,color)

    def update(self,Surf,keys,mouse):
        self.logo_angle = (self.logo_angle+1)%360
        Surf.fill((50,50,50))
        rot_logo = pg.transform.rotozoom(self.logo,self.logo_angle,1)
        rot_logo_rect = rot_logo.get_rect(center=self.logo_rect.center)
        Surf.blit(rot_logo,rot_logo_rect)
        Surf.blit(self.title,self.title_rect)
        if pg.time.get_ticks() - self.timer > 1000/5.0:
            self.blink = not self.blink
            self.timer = pg.time.get_ticks()
        if self.blink:
            Surf.blit(self.ne_key,self.ne_key_rect)

    def get_event(self,event):
        if event.type == pg.KEYDOWN:
            self.next = "MENU"
            self.done = True