import pygame as pg
from .. import setup as su,tools

class Player(object):
    def __init__(self,rect):
        self.rect = pg.Rect(rect)
        self.click = False
        self.image = pg.Surface(self.rect.size).convert()
        self.image.fill((255,0,0))
        self.image.fill((0,50,255),self.rect.inflate((-10,-10)))

    def update(self,Surf):
        if self.click:
            self.rect.move_ip(pg.mouse.get_rel())
        Surf.blit(self.image,self.rect)

class Drag(tools._State):
    def __init__(self):
        tools._State.__init__(self)
        self.Player = Player((0,0,150,150))
        self.Player.rect.center = su.SCREEN_RECT.center
        self.title = self.render_font("Fixedsys500c",40,"Drag With Mouse",(0,255,255))
        self.title_rect = self.title.get_rect(center=(su.SCREEN_RECT.centerx,75))
        self.escape_txt = (self.render_font("Fixedsys500c",20,
                            "[Press ESCAPE to return to menu]",(255,255,0)))
        self.escape_txt_rect = self.escape_txt.get_rect(center=(su.SCREEN_RECT.centerx,500))
        self.blink = False
        self.timer = 0.0
    def render_font(self,font,size,msg,color=(255,255,255)):
        RenderFont = pg.font.Font(su.FONTS[font],size)
        return RenderFont.render(msg,1,color)
    def get_event(self,event):
        if event.type == pg.MOUSEBUTTONDOWN:
            if self.Player.rect.collidepoint(event.pos):
                self.Player.click = True
                pg.mouse.get_rel()
        elif event.type == pg.MOUSEBUTTONUP:
            self.Player.click = False
        elif event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                self.next = "MENU"
                self.done = True
    def cleanup(self):
        self.persist["location"] = self.Player.rect.center
        return tools._State.cleanup(self)
    def startup(self,persistant):
        tools._State.startup(self,persistant)
        if "location" in self.persist:
            self.Player.rect.center = self.persist["location"]
    def update(self,Surf,keys,mouse):
        Surf.fill(0)
        self.Player.update(Surf)
        Surf.blit(self.title,self.title_rect)
        if pg.time.get_ticks() - self.timer > 1000/5.0:
            self.blink = not self.blink
            self.timer = pg.time.get_ticks()
        if self.blink:
            Surf.blit(self.escape_txt,self.escape_txt_rect)