import pygame as pg
import os,sys

from . import setup,tools
from .states import title,menu,fourdir,eightdir,drag

def main():
    RunIt = tools.Control(setup.ORIGINAL_CAPTION)
    state_dict = {"TITLE"  : title.Title(),
                  "MENU"   : menu.Menu(),
                  "FOUR"   : fourdir.FourDir(),
                  "EIGHT"  : eightdir.EightDir(),
                  "DRAG"   : drag.Drag()}
    RunIt.setup_states(state_dict,"TITLE")
    RunIt.main()